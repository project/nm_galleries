<?php
/**
 * @file
 * nm_galleries.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function nm_galleries_taxonomy_default_vocabularies() {
  return array();
}

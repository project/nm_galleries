<?php
/**
 * @file
 * nm_galleries.features.field.inc
 */

/**
 * Implements hook_field_default_fields().
 */
function nm_galleries_field_default_fields() {
  $fields = array();

  // Exported field: 'node-nm_gallery-field_nm_gallery_images'.
  $fields['node-nm_gallery-field_nm_gallery_images'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '-1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_nm_gallery_images',
      'foreign keys' => array(
        'fid' => array(
          'columns' => array(
            'fid' => 'fid',
          ),
          'table' => 'file_managed',
        ),
      ),
      'indexes' => array(
        'fid' => array(
          0 => 'fid',
        ),
      ),
      'locked' => '0',
      'module' => 'image',
      'settings' => array(
        'default_image' => 0,
        'uri_scheme' => 'public',
      ),
      'translatable' => '0',
      'type' => 'image',
    ),
    'field_instance' => array(
      'bundle' => 'nm_gallery',
      'deleted' => '0',
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'hidden',
          'module' => 'flexslider_fields',
          'settings' => array(
            'flexslider_optionset' => 'nodemaker_full',
            'flexslider_reference_img_src' => NULL,
          ),
          'type' => 'flexslider',
          'weight' => '1',
        ),
        'nm_embed_gallery' => array(
          'label' => 'hidden',
          'module' => 'flexslider_fields',
          'settings' => array(
            'flexslider_optionset' => 'nodemaker_full',
            'flexslider_reference_img_src' => NULL,
          ),
          'type' => 'flexslider',
          'weight' => '1',
        ),
        'teaser' => array(
          'label' => 'hidden',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => '2',
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_nm_gallery_images',
      'label' => 'Gallery Images',
      'required' => 0,
      'settings' => array(
        'alt_field' => 1,
        'default_image' => 0,
        'file_directory' => 'gallery-images',
        'file_extensions' => 'png gif jpg jpeg',
        'imagecrop' => array(
          'nodemaker_announcements_teaser' => 0,
          'nodemaker_blog_teaser' => 0,
          'nodemaker_headline_image' => 'nodemaker_headline_image',
          'nodemaker_thumbnail' => 'nodemaker_thumbnail',
        ),
        'max_filesize' => '',
        'max_resolution' => '',
        'min_resolution' => '',
        'title_field' => 1,
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'image',
        'settings' => array(
          'filefield_sources' => array(
            'filefield_sources' => array(
              'attach' => 0,
              'plupload' => 'plupload',
              'reference' => 0,
              'remote' => 0,
            ),
            'source_attach' => array(
              'absolute' => '0',
              'attach_mode' => 'move',
              'path' => 'file_attach',
            ),
            'source_imce' => array(
              'imce_mode' => 0,
            ),
            'source_reference' => array(
              'autocomplete' => '0',
            ),
          ),
          'preview_image_style' => 'nodemaker_thumbnail_small',
          'progress_indicator' => 'throbber',
        ),
        'type' => 'image_image',
        'weight' => '3',
      ),
    ),
  );

  // Exported field: 'node-nm_gallery-field_nm_gallery_visibility'.
  $fields['node-nm_gallery-field_nm_gallery_visibility'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_nm_gallery_visibility',
      'foreign keys' => array(),
      'indexes' => array(
        'value' => array(
          0 => 'value',
        ),
      ),
      'locked' => '0',
      'module' => 'list',
      'settings' => array(
        'allowed_values' => array(
          0 => 'Hidden',
          1 => 'Public',
        ),
        'allowed_values_function' => '',
      ),
      'translatable' => '0',
      'type' => 'list_boolean',
    ),
    'field_instance' => array(
      'bundle' => 'nm_gallery',
      'default_value' => array(
        0 => array(
          'value' => '1',
        ),
      ),
      'deleted' => '0',
      'description' => 'This will hide a gallery from appearing on gallery page(s) and views that display recent, random galleries, etc. These galleries will, however, be available to be embedded inside a node with a node reference field.',
      'display' => array(
        'default' => array(
          'label' => 'hidden',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => '4',
        ),
        'nm_embed_gallery' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
        'teaser' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_nm_gallery_visibility',
      'label' => 'Gallery Visibility',
      'required' => 1,
      'settings' => array(
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'options',
        'settings' => array(),
        'type' => 'options_buttons',
        'weight' => '5',
      ),
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Gallery Description');
  t('Gallery Images');
  t('Gallery Visibility');
  t('Headline Image');
  t('Related Content');
  t('Tags');
  t('This will hide a gallery from appearing on gallery page(s) and views that display recent, random galleries, etc. These galleries will, however, be available to be embedded inside a node with a node reference field.');

  return $fields;
}
